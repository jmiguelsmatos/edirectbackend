var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');
var User = require('../models/user');



router.get('/',User.isAuthenticated, function (req, res, next) {
    User.find({}, function(err, users) {
        if (err) next(new Error("Cannot get users")); else res.send({response:users});
    });
});

router.post('/', function(req, res, next) {
    var newUser = User(req.body);
    newUser.save(function(err, user) {
        if (err) res.send({inError: true, errorCode:err.code, message: 'Cannot create user'});
        else res.send({inError: false, response:user});
    });
});

router.delete('/:id',User.isAuthenticated, function (req, res, next) {
    User.findOne({ _id: new mongodb.ObjectID(req.params.id)}, function(err, user) {
        if (err) next(new Error("Cannot Delete"));else {
            var userDelete = User(user);
            userDelete.remove();
            res.send({response: user})
        }
    });
});

router.put('/:id',User.isAuthenticated, function (req, res, next) {
    User.findOneAndUpdate({ _id: new mongodb.ObjectID(req.params.id)}, req.body, function(err, user) {
        if (err) next(new Error("Cannot update")); else res.send({response:user});
    });
});




module.exports = router;
