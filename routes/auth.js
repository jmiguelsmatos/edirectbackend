var express = require('express');
var router = express.Router();
var User = require('../models/user');

router.post('/login', function (req, res, next) {
    User.authenticate(req.body.username, req.body.password, function (err, user) {
        if(err) res.send({inError:true, response: 'Wrong Credentials'}); else {
            if(user !== undefined){
                req.session.userId = user._id;
                res.send({inError:false, response:{sessionId:req.session.userId, userId: user.id, username: user.username}});
            }else{
                res.send({inError:true, response: 'Wrong Credentials'})
            }
        }
    });
});

    router.post('/ping', User.isAuthenticated, function (req, res, next) {
        res.send({inError: false, response: true});
    });

    router.post('/logout', function (req, res, next) {
        req.session.userId = null;
        res.send({inError:false, response:'Success'});
    });


    module.exports = router;
