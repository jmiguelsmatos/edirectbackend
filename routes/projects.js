var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');
var Project = require('../models/projects');
var User = require('../models/user');


router.get('/:id',User.isAuthenticated, function (req, res, next) {
    Project.aggregate([
        {
            $match:
                {'user_id': req.params.id}
        },
        { $lookup:
            {
                from: 'tasks',
                localField: '_id',
                foreignField: 'project_id',
                as: 'tasks'
            }
        }
    ]).exec(function(err, projects) {
        if (err) throw err;
        res.send(projects);
    });

});

router.get('/',User.isAuthenticated, function (req, res, next) {
    Project.find({}, function(err, projects) {
        if (err) next(new Error("Cannot get users")); else res.send({response:projects});
    });

});

router.post('/',User.isAuthenticated,function(req, res, next) {
    var newProject = Project(req.body);
    newProject.save(function(err, project) {
        if (err) res.send({inError: true, errorCode:err.code, message: 'Cannot create project'});
        else res.send({inError: false, response:project});
    });
});

router.delete('/:id',User.isAuthenticated, function (req, res, next) {
    Project.findOne({ _id: new mongodb.ObjectID(req.params.id)}, function (err, project) {
        if (err) next(new Error("Cannot update"));else {
            var deleteProject = Project(project);
            deleteProject.remove();
            res.send({response: project})
        }
    });
});

router.put('/:id',User.isAuthenticated, function (req, res, next) {
    Project.findOneAndUpdate({ _id: new mongodb.ObjectID(req.params.id)}, req.body, function(err, project) {
        if (err) next(new Error("Cannot update")); else res.send({response:project});
    });
});




module.exports = router;
