var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');
var Task = require('../models/tasks');
var Project = require('../models/projects');
var User = require('../models/user');


router.get('/',User.isAuthenticated, function (req, res, next) {
    Task.find({}, function(err, tasks) {
        if (err) next(new Error("Cannot get users")); else res.send({response:tasks});
    });
});

router.get('/:id', User.isAuthenticated, function (req, res, next) {
    Task.find({ project_id: new mongodb.ObjectID(req.params.id)}, function(err, task) {
        if (err) next(new Error("Cannot remove")); else res.send({response:task});
    });
});

router.post('/',User.isAuthenticated,function(req, res, next) {
        var newTask = Task(req.body);
        newTask.save(function(err, task) {
        if (err) res.send({inError: true, errorCode:err.code, message: 'Cannot create task'});
        else res.send({inError: false, response:task});
    });
});

router.delete('/:id',User.isAuthenticated, function (req, res, next) {
    Task.findOneAndRemove({ _id: new mongodb.ObjectID(req.params.id)}, function(err, task) {
        if (err) next(new Error("Cannot remove")); else res.send({response:task});
    });
});

router.put('/:id',User.isAuthenticated, function (req, res, next) {
    Task.findOneAndUpdate({ _id: new mongodb.ObjectID(req.params.id)}, req.body, function(err, task) {
        if (err) next(new Error("Cannot update")); else res.send({response:task});
    });
});




module.exports = router;
