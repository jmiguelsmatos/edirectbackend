var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var tasksSchema = new Schema({
    name: { type: String, required: true},
    project_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Project'},
    created_at: Date,
    updated_at: Date,
    state: {type: Number}
});


tasksSchema.pre('save', function(next) {

    var currentDate = new Date();


    this.updated_at = currentDate;


    if (!this.created_at)
        this.created_at = currentDate;

    if(!this.state){
        this.state = 0;
    }

    next();
});



var Task = mongoose.model('Task', tasksSchema);


module.exports = Task;