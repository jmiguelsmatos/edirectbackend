var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var Project = require('../models/projects');
var Task = require('../models/tasks');

var userSchema = new Schema({
    name: String,
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    email: { type: String},
    address: { type: String},
    created_at: Date,
    updated_at: Date
});


// on every save, add the date and encrypts the password
userSchema.pre('save', function(next) {
    var user = this;
    bcrypt.hash(user.password, 10, function (err, hash) {
        if (err) {
            return next(err);
        }
        user.password = hash;


        var currentDate = new Date();
        user.updated_at = currentDate;

        if (!user.created_at)
            user.created_at = currentDate;

        next();
    });
});

userSchema.post('remove', function (next) {
    Project.find({ user_id: this._id }, function (err, projects) {
      projects.forEach(function (data) {
          data.remove();
      })
    });
});

userSchema.statics.authenticate = function (username, password, callback) {
    User.findOne({ username: username })
        .exec(function (err, user) {
            if (err) {
                return callback(err)
            } else if (!user) {
                err = new Error('User not found.');
                err.status = 401;
                return callback(err);
            }
            bcrypt.compare(password, user.password, function (err, result) {
                if (result === true) {
                    return callback(null, user);
                } else {
                    return callback(err);
                }
            })
        });
};

userSchema.statics.isAuthenticated = function(req, res, next) {

    //todo: change session ID to an actual hash
    if (!!req.session.userId && req.session.userId === req.get('sessionID'))
        return next();

    var error = new Error();
    error.status = 503;
    next(error);
};


var User = mongoose.model('User', userSchema);

module.exports = User;