var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Task = require('../models/tasks');

var projectSchema = new Schema({
    name: { type: String, required: true, unique: true },
    user_id: { type: String, required: true},
    created_at: Date,
    updated_at: Date,
    tasks: [{ type: Schema.ObjectId, ref: 'Task'}]
});

projectSchema.pre('save', function(next) {
        var currentDate = new Date();

        this.updated_at = currentDate;

        if (!this.created_at)
            this.created_at = currentDate;

        next();
});

projectSchema.post('remove', function (next) {
        Task.remove({ project_id: this._id }).exec();
});


var Project = mongoose.model('Project', projectSchema);


module.exports = Project;