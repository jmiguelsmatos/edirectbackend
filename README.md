# backendproject [![NPM version](https://badge.fury.io/js/backendproject.svg)](https://npmjs.org/package/backendproject) [![Build Status](https://travis-ci.org/jmiguelsmatos/backendproject.svg?branch=master)](https://travis-ci.org/jmiguelsmatos/backendproject)

> eDirect code challenge Backend

## Installation

After installing MongoDB
Run 

```sh
mongod
```
Then clone the project 
```sh
git clone https://jmiguelsmatos@bitbucket.org/jmiguelsmatos/edirectbackend.git
```

Install it with following command
```sh
npm install
```


## Usage

Ready to go!
Run

```js
npm start
```

## License

MIT � [Miguel Matos]()
